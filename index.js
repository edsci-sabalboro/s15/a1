let userAccount = {
    firstName: 'Denz Christian',
    lastName: 'Sabalboro',
    age: 17,
    hobbies: ['Practicing coding skills', 'Reading novels and books', 'Watching Ghibli Studio'],
    workAddress: {
        houseNumber: 'Blk 43 Lot 28',
        street: 'Rose',
        city: 'Caloocan',
        state: 'Philippines'
    }
};

console.log(userAccount);